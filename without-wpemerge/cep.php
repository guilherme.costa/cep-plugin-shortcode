<?php
/*
 *Plugin Name: Cep-Plugin
 * Author: Guilherme Costa
 * Description: Plugin para retornar o endereço do cep digitado.
 * Version: 1.0
 * Author URI:https://github.com/guilhermehrcosta
 */
if(!function_exists('add_action')){
	return exit(0);
}
include ('class/Cep.php');
include('class/DB.php');
Cep::init();

function createTableCEP(){
	$db = new DB();
	$db->createTable();
}
function deleteTableCEP(){
	$db = new DB();
	$db->deleteTable();
}
register_activation_hook (__FILE__, 'createTableCEP');
register_uninstall_hook(__FILE__,'deleteTableCEP');