<?php
/**
 * Created by PhpStorm.
 * User: Guilherme
 * Date: 16/08/18
 * Time: 12:28
 */
require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );


class Cep {
	private static $nameFunctioRenderCEP = array('CEP','renderPageCep');

	public static function getArrFunctionShortCode() {
		return array('CEP','getRenderInputFields');
	}

	public static function init(){
		add_action( 'admin_menu', array('CEP','addPageMenu') );
		add_shortcode('cep',self::getArrFunctionShortCode());
		add_action( 'rest_api_init', array('CEP','registerRouteApi'));

	}
	public function addPageMenu (){
		add_menu_page('Cep Consultados',
			'CEP','manage_options','cep-menu',self::$nameFunctioRenderCEP);
	}

	public function renderPageCep(){
		global  $wpdb;
		$nameDB = $wpdb->prefix.'cep';
		$query =  $wpdb->get_results("select * from $nameDB");
		?>
        <h3>CEP consultados.</h3>
        <table style="width: 100%!important;">
            <thead>
            <tr>
                <th style="width: 10%!important;">CEP</th>
                <th style="width: 20%!important;">Bairro</th>
                <th style="width: 30%!important;">Logradouro</th>
                <th style="width: 30%!important;">Município</th>
                <th style="width: 10%!important;">UF</th>
            </tr>
            </thead>
            <tbody>
			<?php
			foreach ($query as $q){
				?>
                <tr>
                    <th><?php echo $q->cep?></th>
                    <th><?php echo $q->bairro?></th>
                    <th><?php echo $q->logradouro?></th>
                    <th><?php echo $q->municipio?></th>
                    <th><?php echo $q->uf?></th>
                </tr>
				<?php
			}

			?>
            </tbody>
        </table>
		<?php
	}

	public function getRenderInputFields(){
		?>
        <div class="wrap">
            <label for="cep">Cep:</label>
            <input type="text"  name="cep" id="cep">

            <label for="logradouro">Logradouro:</label>
            <input type="text"  name="logradouro" id="logradouro" disabled>

            <label for="bairro">Bairro:</label>
            <input type="text"  name="bairro" id="bairro" disabled>

            <label for="municipio">Munícipio:</label>
            <input type="text"  name="municipio" id="municipio" disabled>

            <label for="uf">UF:</label>
            <input type="text"  name="uf" id="uf" disabled/>

            <br/>
            <br/>
            <button type="button" id="btnBuscarCEP"> Buscar o CEP</button>
        </div>

        <script>
            jQuery(document).ready(function( $ ) {
                function clearFields(){
                    $("#logradouro").val("");
                    $("#bairro").val("");
                    $("#municipio").val("");
                    $("#uf").val("");
                }
                function getDadosCorreios(cep){
                    $.getJSON("https://viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
                        if (!("erro" in dados)) {
                            $("#logradouro").val(dados.logradouro);
                            $("#bairro").val(dados.bairro);
                            $("#municipio").val(dados.localidade);
                            $("#uf").val(dados.uf);
                            var url = "<?php echo get_home_url().'/wp-json/cep_api/Insert'?>";
                            $.post(url,dados,function () {
                                console.log("it's fine.");
                            });
                        }
                        else {
                            clearFields();
                            alert("CEP não encontrado.");
                        }
                    });
                }

                $('#cep').keypress(function (key) {
                    if(key.keyCode === 13){
                        $('#btnBuscarCEP').click();
                    }
                });

                $('#btnBuscarCEP').click(function () {
                    var cep = $('#cep').val().replace(/\D/g, '');
                    if (cep !== "") {
                        var validatecep = /^[0-9]{8}$/;
                        if (validatecep.test(cep)) {
                            $("#logradouro").val("...");
                            $("#bairro").val("...");
                            $("#municipio").val("...");
                            $("#uf").val("...");
                            var url = "<?php echo get_home_url().'/wp-json/cep_api/GetCEP'?>";
                            $.post(url,{cep:cep} ,function (dados) {
                                if(!dados.success){
                                    getDadosCorreios(cep);
                                }else{
                                    $("#logradouro").val(dados.cep.logradouro);
                                    $("#bairro").val(dados.cep.bairro);
                                    $("#municipio").val(dados.cep.municipio);
                                    $("#uf").val(dados.cep.uf);
                                }
                            });


                        }else{
                            alert("CEP inválido");
                        }
                    }
                });

            });
        </script>
		<?php
	}

	public function registerRouteApi(){
		register_rest_route( 'cep_api',  '/GetCEP', array(
			array(
				'methods'   => 'POST',
				'callback'  => array( Cep::class, 'getGEP' )
			),
		));
		register_rest_route( 'cep_api', '/Insert', array(
			array(
				'methods'   => 'POST',
				'callback'  => array( Cep::class, 'addCEP' )
			),
		));

		register_rest_route( 'cep_api',  '/HelloWorld', array(
			array(
				'methods'   => 'GET',
				'callback'  => array( Cep::class, 'hello' )
			),
		));
	}

	public  function getGEP($request){
		$cep  = str_replace('-','',$request['cep']);
		if(self::checkCEPExist($cep)){
			global  $wpdb;
			$nameDB = $wpdb->prefix.'cep';
			$query =  $wpdb->get_results("select * from $nameDB where cep = $cep");
			return rest_ensure_response(array('success'=>true,'cep'=>$query[0]));
        }
		return rest_ensure_response(array('success'=>false));
	}

	public function hello(){
		$message = array('message'=>'Olá Mundo.','success'=>true);
		return rest_ensure_response( $message );
	}

	public function addCep($request){
		$cep  = str_replace('-','',$request['cep']);
		$logradouro = $request['logradouro'];
		$bairro = $request['bairro'];
		$localidade = $request['localidade'];
		$uf = $request['uf'];
		if(!self::checkCEPExist($cep)){
			$db = new DB();
			$db->insertDB($cep,$logradouro,$localidade,$bairro,$uf);
			$message = array('message'=>'Add com sucesso. ','sucess'=>true);
		}else{
			$message = array('message'=>'Já existe. ','sucess'=>true);
		}
		return rest_ensure_response( $message );

	}

	private function checkCEPExist($cep){
		global  $wpdb;
		$nameDB = $wpdb->prefix.'cep';
		$query =  $wpdb->get_results("select * from $nameDB where cep = $cep");
		return sizeof($query) > 0;
	}





}