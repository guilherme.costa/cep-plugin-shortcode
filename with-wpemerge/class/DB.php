<?php
/**
 * Created by PhpStorm.
 * User: guilherme
 * Date: 16/08/18
 * Time: 14:05
 */

class DB {
	private $db;
	private $version = "1.0";
	private $name_table = '';


	public function __construct() {
		global $wpdb;
		$this->db         = $wpdb;
		$this->name_table = $this->db->prefix . 'cep';
	}


	public function createTable() {
		if ( get_option( "version_cep_db" ) == '' ) {
			$table_name = $this->getNameTable();
			$sql        = "CREATE TABLE $table_name (
 id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  cep varchar(8),
  logradouro varchar(100),
  municipio varchar(100),
  bairro varchar(100),
  uf varchar(2)
) ";
			$this->executeSQL( $sql );
			add_option( 'version_cep_db', $this->version );
		}


	}

	public function deleteTable() {
		delete_option( 'version_cep_db' );
		$table_name = $this->getNameTable();
		$sql        = "DROP TABLE IF EXISTS $table_name";
		$this->executeSQL( $sql );
	}

	private function executeSQL( $sql ) {
		dbDelta( $sql );
	}

	public function getNameTable() {
		return $this->name_table;
	}

	public function getInstance() {
		return $this->db;
	}

	public function insertDB( $cep, $logradouro,$municipio,$bairro,$uf ) {
		$this->db->insert(
			$this->name_table,
			array(
				'cep' => $cep,
				'logradouro' => $logradouro,
				'municipio' => $municipio,
				'bairro' => $bairro,
				'uf' => $uf,
			));
	}

	public function deleteDB( $cep, $logradouro,$municipio,$bairro,$uf ) {
		$this->db->delete(
			$this->name_table,
			array(
				'cep' => $cep,
				'logradouro' => $logradouro,
				'municipio' => $municipio,
				'bairro' => $bairro,
				'uf' => $uf,
			));
	}
}